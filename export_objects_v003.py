import bpy
 
bl_info = {
    "name": "Easy Object",
    "author": "Kursad Karatas/COdemax",
    "version": (0, 1, 0),
    "blender": (2, 6 ,6),
    "location": "View3D > UI panel >Easy Export",
    "description": "Convenient object exporting",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"}


class EasyExport(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.easyexport_operator"
    bl_label = "Easy Export"
 
    @classmethod
    def poll(cls, context):
        return context.selected_objects
 
    def execute(self, context):
        easyExport(context)
        return {"FINISHED"}
     
    def draw_filepath_prop(self, context):
        if context.object.type == "MESH":
            self.layout.prop(context.object, "filepath")
    bpy.types.OBJECT_PT_context_object.append(draw_filepath_prop)
     



def menu_draw(self, context): 
    self.layout.operator_context = 'INVOKE_REGION_WIN' 
    self.layout.operator(EasyExport.bl_idname, "EasyExport") 
       
def register():
    bpy.utils.register_class(EasyExport)
    bpy.types.Object.filepath = bpy.props.StringProperty(name="Filepath", subtype="FILE_PATH")
    bpy.types.VIEW3D_MT_select_object.prepend(menu_draw) 

 
def unregister():
    bpy.utils.unregister_class(EasyExport)
    del bpy.types.Object.filepath
     
 
#def easyExport(context):
def easyExport(context):
    for ob in bpy.context.scene.objects:
        if ob.type != "MESH":
            continue
       
        bpy.ops.export_scene.obj({"selected_objects": [ob], # just one object at a time, using an override so we can keep selections
                                  "window": context.window, # suppress PyContext warnings
                                  "scene": context.scene,
                                  "blend_data": context.blend_data},
                                 filepath=ob.filepath,
                                 use_selection=True,
                                 # add more params here if needed
                                 )

 
if __name__ == "__main__":
    register()